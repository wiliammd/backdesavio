package com.wilsoftware.notafiscal.commons.validator;


import com.wilsoftware.notafiscal.commons.EntityBase;

public interface Validator<ENTITY extends EntityBase, EXCEPTION extends RuntimeException> {

    boolean isValid(ENTITY entity);

    EXCEPTION getException(ENTITY entity);

    default void validate(ENTITY entity) {
        if (!isValid(entity)) {
            throw getException(entity);
        }
    }
}
