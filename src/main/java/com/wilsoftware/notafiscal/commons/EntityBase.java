package com.wilsoftware.notafiscal.commons;

import java.time.LocalDateTime;

public interface EntityBase {
    String getId();
    LocalDateTime getDateCreate();
    String getSessaoCreate();
    LocalDateTime getDateUpdate();
    String getSessaoUpdate();
}