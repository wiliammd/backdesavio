package com.wilsoftware.notafiscal.commons;

import java.time.LocalDateTime;

public interface EntityBaseInteger {
    Integer getId();

    LocalDateTime getDateCreate();

    String getSessaoCreate();

    LocalDateTime getDateUpdate();

    String getSessaoUpdate();
}