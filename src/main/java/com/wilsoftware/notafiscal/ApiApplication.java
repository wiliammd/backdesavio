package com.wilsoftware.notafiscal;

import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import com.wilsoftware.notafiscal.usuarios.domain.UsuarioRepository;
import com.wilsoftware.notafiscal.util.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;


@EnableScheduling
@SpringBootApplication
@EnableJpaRepositories("com.wilsoftware.notafiscal")
public class ApiApplication implements CommandLineRunner {

	@Autowired
	private BCryptPasswordEncoder pe;

	@Autowired
	private UsuarioRepository usuarioRepository;




	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}



	@Override
	public void run(String... args) throws Exception {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		List<Usuario> listUsuairo = usuarioRepository.findAll();
		if (listUsuairo.isEmpty()) {

			Usuario user1 = Usuario.builder()
					.id(IdGenerator.get())
					.nome("Admin")
					.codigo(1)
					.email("wil@email.com.br")
					.usuario("wiliammd")
					.ativo(true)
					.dateCreate(LocalDateTime.now())
					.sessaoCreate("1")
					.senha(pe.encode("123"))
					.build();
			usuarioRepository.save(user1);

		}

	}
}
