package com.wilsoftware.notafiscal;

import com.wilsoftware.notafiscal.security.UserSS;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Map;

public class UserService {
	
	public static UserSS authenticated() {
		try {
			return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		catch (Exception e) {
			return null;
		}
	}
}
