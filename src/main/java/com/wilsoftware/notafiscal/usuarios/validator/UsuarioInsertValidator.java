package com.wilsoftware.notafiscal.usuarios.validator;

import com.wilsoftware.notafiscal.commons.validator.Validator;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;

public interface UsuarioInsertValidator<EXCEPTION extends RuntimeException> extends Validator<Usuario, EXCEPTION> {

}
