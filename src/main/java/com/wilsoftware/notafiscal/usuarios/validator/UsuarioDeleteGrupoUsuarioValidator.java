package com.wilsoftware.notafiscal.usuarios.validator;

import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.grupo.domain.Grupo;
import com.wilsoftware.notafiscal.grupo.domain.GrupoRepository;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UsuarioDeleteGrupoUsuarioValidator implements UsuarioDeleteValidator<ObjectNotFoundException> {
    @Autowired
    private GrupoRepository repository;


    @Override
    public boolean isValid(Usuario entity) {
        List<Grupo> byId = repository.findByUsuarios_id(entity.getId());
        if (byId.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public ObjectNotFoundException getException(Usuario entity) {
        String[] array = {entity.getId()};
        throw new ObjectNotFoundException("{wilsoftware.usuario.exception.UsuarioJaCadastradoNoGrupo}");
    }

    @Override
    public Integer getOrder() {
        return 1;
    }

}
