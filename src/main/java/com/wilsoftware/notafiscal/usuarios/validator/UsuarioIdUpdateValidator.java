package com.wilsoftware.notafiscal.usuarios.validator;

import com.wilsoftware.notafiscal.UserService;
import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import com.wilsoftware.notafiscal.usuarios.domain.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class UsuarioIdUpdateValidator implements UsuarioUpdateValidator<ObjectNotFoundException> {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public boolean isValid(Usuario entity) {

        Optional<Usuario> byId = repository.findById(entity.getId());
        if(byId.isPresent()){
            entity.setSenha(byId.get().getSenha());
            entity.setCodigo(byId.get().getCodigo());
            entity.setDateCreate(byId.get().getDateCreate());
            entity.setSessaoCreate(byId.get().getSessaoCreate());
            entity.setDateUpdate(LocalDateTime.now());
            entity.setSessaoUpdate(UserService.authenticated().getId());
        }
        return byId.isPresent();

    }

    @Override
    public ObjectNotFoundException getException(Usuario entity) {
        String[] array = { entity.getId() };
        throw new ObjectNotFoundException("{wilsoftware.usuario.exception.idNaoEncontrado}", array);
    }
}
