package com.wilsoftware.notafiscal.usuarios.validator;

import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import com.wilsoftware.notafiscal.usuarios.domain.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UsuarioIdInsertValidator implements UsuarioInsertValidator<ObjectNotFoundException> {

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private BCryptPasswordEncoder pe;

    @Override
    public boolean isValid(Usuario entity) {

        entity.setSenha(pe.encode("trocar"));
        return true;
    }

    @Override
    public ObjectNotFoundException getException(Usuario entity) {
        String[] array = { entity.getId() };
        throw new ObjectNotFoundException("{wilsoftware.usuario.exception.idNaoEncontrado}", array);
    }
}
