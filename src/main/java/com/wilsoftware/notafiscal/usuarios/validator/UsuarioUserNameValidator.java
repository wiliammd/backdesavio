package com.wilsoftware.notafiscal.usuarios.validator;

import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import com.wilsoftware.notafiscal.usuarios.domain.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsuarioUserNameValidator implements UsuarioInsertValidator<ObjectNotFoundException>, UsuarioUpdateValidator<ObjectNotFoundException> {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public boolean isValid(Usuario entity) {

        return repository.findByUsuario(entity.getUsuario())
                .map(entity::temMesmoId)
                .orElse(true);

    }

    @Override
    public ObjectNotFoundException getException(Usuario entity) {
        throw new ObjectNotFoundException("{wilsoftware.usuario.exception.nomeUsuarioJaCadastradoOutroUsuario}");
    }
}
