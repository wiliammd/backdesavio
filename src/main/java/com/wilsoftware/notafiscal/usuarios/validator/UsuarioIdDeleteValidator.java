package com.wilsoftware.notafiscal.usuarios.validator;

import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import com.wilsoftware.notafiscal.usuarios.domain.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UsuarioIdDeleteValidator implements UsuarioDeleteValidator<ObjectNotFoundException> {
    @Autowired
    private UsuarioRepository repository;

    @Override
    public boolean isValid(Usuario usuario) {
        Optional<Usuario> byId = repository.findById(usuario.getId());
        if (byId.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public ObjectNotFoundException getException(Usuario usuario) {
        String[] array = {usuario.getId()};
        throw new ObjectNotFoundException("{wilsoftware.usuario.exception.idNaoEncontrado}", array);
    }

    @Override
    public Integer getOrder() {
        return 1;
    }
}
