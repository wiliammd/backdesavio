package com.wilsoftware.notafiscal.usuarios.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, String> {

    Usuario findByid(String id);

    Optional<Usuario> findByEmail(String email);

    Optional<Usuario> findByUsuario(String usuario);

    Optional<Usuario> findByCodigo(Integer codigo);

    @Query("SELECT coalesce(max(ch.codigo), 0) FROM Usuario ch")
    Integer getMaxCodigo();

    @Query("SELECT u from Usuario u where u.ativo = :ativo")
    Page<Usuario> findByAtivo(Pageable var1, boolean ativo);

    Page<Usuario> findByCodigoOrderByCodigoAsc(Integer codigo, Pageable pageRequest);

    Page<Usuario> findByNomeIgnoreCaseLikeOrEmailIgnoreCaseLikeOrderByCodigoAsc(String filter, String filter1, String filter2, String filter3, PageRequest pageRequest);

    Optional<Usuario> findByGrupoUsuario_IdAndCodigoOrderByCodigoAsc(String grupoId, Integer codigo);

    List<Usuario> findByGrupoUsuario_id(String id);
}
