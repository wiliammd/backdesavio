package com.wilsoftware.notafiscal.usuarios.domain;

public enum Perfil {
	
	ADMIN,
	CLIENTE;

}
