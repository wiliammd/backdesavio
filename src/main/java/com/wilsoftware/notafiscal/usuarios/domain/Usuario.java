package com.wilsoftware.notafiscal.usuarios.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wilsoftware.notafiscal.commons.EntityBase;
import com.wilsoftware.notafiscal.grupo.domain.Grupo;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Entity
@Getter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USUARIO")
public class Usuario implements EntityBase, Cloneable {

    @Id
    @Setter
    private String id;

    @Setter
    @NotNull(message = "codigo")
    private Integer codigo;

    @NotNull(message = "nome")
    private String nome;

    @NotNull(message = "usuario")
    private String usuario;

    private String email;

    @JsonIgnore
    @Setter
    private String senha;

    private boolean ativo = true;

    @NotNull(message = "{wilsoftware.default.erro.dataInsert}")
    @Setter
    private LocalDateTime dateCreate;

    @NotNull(message = "{wilsoftware.default.erro.sessaoInsert}")
    @Setter
    private String sessaoCreate;

    @Setter
    private LocalDateTime dateUpdate;

    @Setter
    private String sessaoUpdate;

    @ManyToMany
    @JoinTable(name = "USUARIO_GRUPO",
            joinColumns = {@JoinColumn(name = "ID_USUARIO")},
            inverseJoinColumns = {@JoinColumn(name = "ID_GRUPO")})
    private List<Grupo> grupoUsuario;

    public boolean temMesmoId(Usuario usuario) {
        return this.id.equals(usuario.getId());
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
