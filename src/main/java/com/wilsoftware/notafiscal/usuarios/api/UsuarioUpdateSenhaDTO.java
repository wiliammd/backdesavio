package com.wilsoftware.notafiscal.usuarios.api;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioUpdateSenhaDTO {

    private String idUsuario;
    private String oldSenha;
    private String newSenha;
    private String newSenhaConfirmacao;

}
