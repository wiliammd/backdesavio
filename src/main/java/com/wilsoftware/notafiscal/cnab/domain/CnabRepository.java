package com.wilsoftware.notafiscal.cnab.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CnabRepository extends JpaRepository<Cnab, String> {
}
