package com.wilsoftware.notafiscal.cnab.domain;

import com.wilsoftware.notafiscal.UserService;
import com.wilsoftware.notafiscal.util.ConvertStringToLocalDate;
import com.wilsoftware.notafiscal.util.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CnabService {
    @Autowired
    private CnabRepository repository;

    public Page<Cnab> findAll(int page, int pageSize){
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by("dateCreate").descending());
        return repository.findAll(pageRequest);
    }
    public List<Cnab> insert(MultipartFile file){


        return repository.saveAll(transformarLista(file));
    }

    private List<Cnab>transformarLista(MultipartFile file){
        List<String> quebraLinha = new ArrayList<>();
        List<Cnab> cnabList = new ArrayList<>();
        try {
            String content = new String(file.getBytes(), StandardCharsets.UTF_8);
            quebraLinha = List.of(content.split("\n"));
            quebraLinha.forEach(item->{
                TransacaoEnum result = null;
                for(TransacaoEnum enumItem : TransacaoEnum.values()){
                    if(Integer.parseInt(item.substring(0,1)) == enumItem.getTipo()){
                         result = enumItem;
                    }
                }

                cnabList.add(Cnab.builder()
                                .id(IdGenerator.get())
                                .dataOcorrencia(ConvertStringToLocalDate.converterStringToLocalDate(item.substring(1,9) +item.substring(42,48)))
                                .cartao(item.substring(30,42))
                                .cpf(item.substring(19,30))
                                .dateCreate(LocalDateTime.now())
                                .sessaoCreate(UserService.authenticated().getId())
                                .nomeLoja(item.substring(62,item.length() -2))
                                .nomeRepresenteLoja(item.substring(48,62).trim())
                                .transacao(result)
                                .valor(Double.parseDouble(item.substring(9,19)) / 100.00)
                        .build());

            });
        }catch (IOException error){

        }
        return cnabList;
    }
}
