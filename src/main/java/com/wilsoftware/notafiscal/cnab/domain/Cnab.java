package com.wilsoftware.notafiscal.cnab.domain;

import com.wilsoftware.notafiscal.commons.EntityBase;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Builder
@Setter
@Getter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cnab")
@Entity
public class Cnab implements EntityBase {
    @Id
    @ApiModelProperty("Id Interno do Cnab")
    @Column(length = 40)
    private String id;

    @Enumerated(EnumType.STRING)
    private TransacaoEnum transacao;

    private LocalDateTime dataOcorrencia;

    private Double valor;

    private String cpf;

    private String cartao;

    private String nomeRepresenteLoja;

    private String nomeLoja;


    @NotNull(message = "{wilsoftware.default.erro.dataInsert}")
    private LocalDateTime dateCreate;

    @NotNull(message = "{wilsoftware.default.erro.sessaoInsert}")
    private String sessaoCreate;

    private LocalDateTime dateUpdate;

    private String sessaoUpdate;
}
