package com.wilsoftware.notafiscal.cnab.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TransacaoEnum {
    DEBITO(1,"Débito","Entrada","+"),
    BOLETO(2,"Boleto","Saída","-"),
    FINANCIMENTO(3,"Financiamento","Saída","-"),
    CREDITO(4,"Crédito","Entrada","+"),
    RECEBIMENTOEMPRESTIMO(5,"Recebimento Empréstimo","Entrada","+"),
    VENDAS(6,"Vendas","Entrada","+"),
    RECEBIMENTOTED(7,"Recebimento TED","Entrada","+"),
    RECEBIMENTODOC(8,"Recebimento DOC","Entrada","+"),
    ALUGUEL(9,"Aluguel","Saída","-");

    private final int tipo;
    private final String descricao;
    private final String natureza;
    private final String sinal;
}
