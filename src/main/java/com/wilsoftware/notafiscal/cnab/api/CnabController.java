package com.wilsoftware.notafiscal.cnab.api;

import com.wilsoftware.notafiscal.cnab.domain.Cnab;
import com.wilsoftware.notafiscal.cnab.domain.CnabService;
import com.wilsoftware.notafiscal.commons.ApiCollectionResponse;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/enviarArquivo")
public class CnabController {
    @Autowired
    private CnabService service;
    @PostMapping
    @ApiOperation("Transformar arquivo txt em classe java e inserir no banco")
    public List<Cnab> lerArquivo(@RequestPart(name = "file", required = false) MultipartFile file) throws IOException {
        List<Cnab> retorno = service.insert(file);

        return retorno;
    }

    @GetMapping
    @ApiOperation("Recupera lista de Usuários")
    public ResponseEntity<ApiCollectionResponse> findAll(@RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "pageSize", defaultValue = "20") int pageSize, @RequestParam(value = "filter", defaultValue = "") String filter, @RequestParam(value = "grupoId", defaultValue = "") String grupoId) {
        Page<Cnab> cnabs = service.findAll(page, pageSize);
        return ResponseEntity.ok()
                .body(ApiCollectionResponse.builder()
                        .items(cnabs.getContent()).hasNext(cnabs.hasNext()).build()
                );
    }

}
