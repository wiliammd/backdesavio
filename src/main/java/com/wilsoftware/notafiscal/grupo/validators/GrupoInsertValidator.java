package com.wilsoftware.notafiscal.grupo.validators;

import com.wilsoftware.notafiscal.commons.validator.Validator;
import com.wilsoftware.notafiscal.grupo.domain.Grupo;

public interface GrupoInsertValidator<EXCEPTION extends RuntimeException> extends Validator<Grupo, EXCEPTION> {

}
