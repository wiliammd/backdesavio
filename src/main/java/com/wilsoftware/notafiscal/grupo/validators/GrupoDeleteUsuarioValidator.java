package com.wilsoftware.notafiscal.grupo.validators;

import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.grupo.domain.Grupo;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import com.wilsoftware.notafiscal.usuarios.domain.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GrupoDeleteUsuarioValidator implements GrupoDeleteValidator<ObjectNotFoundException> {
    @Autowired
    private UsuarioRepository repository;

    @Override
    public boolean isValid(Grupo entity) {
        List<Usuario> byId = repository.findByGrupoUsuario_id(entity.getId());
        if (byId.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public ObjectNotFoundException getException(Grupo entity) {
        String[] array = {entity.getId()};
        throw new ObjectNotFoundException("{wilsoftware.grupo.exception.grupoJaCadastradoNoUsuario}");
    }

    @Override
    public Integer getOrder() {
        return 1;
    }

}
