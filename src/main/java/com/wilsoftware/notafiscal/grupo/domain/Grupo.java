package com.wilsoftware.notafiscal.grupo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wilsoftware.notafiscal.commons.EntityBase;
import com.wilsoftware.notafiscal.grupoPermissao.domain.GrupoPermissao;
import com.wilsoftware.notafiscal.usuarios.domain.Usuario;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "GRUPO")
@Entity
public class Grupo implements EntityBase, Cloneable {

    @Id
    @Setter
    @ApiModelProperty("Id Interno do Grupo de Usuário")
    private String id;

    @NotNull(message = "{wilsoftware.grupo.exception.codigoNaoInformado}")
    @ApiModelProperty("Código do Grupo de Usuário")
    @Setter
    private Integer codigo;

    @NotNull(message = "{wilsoftware.grupo.exception.descricaoNaoInformado}")
    @ApiModelProperty("Descrição do Grupo de Usuário")
    private String descricao;

    @ApiModelProperty("Grupo de usuário Ativo=true ou Desativado=false")
    private boolean ativo = true;

    @JsonIgnore
    @ManyToMany(mappedBy = "grupoUsuario")
    private List<Usuario> usuarios;

    @ManyToMany
    @JoinTable(name = "GRUPO_USUARIO_PERMISSAO",
            joinColumns = {@JoinColumn(name = "ID_GRUPO_USUARIO")},
            inverseJoinColumns = {@JoinColumn(name = "ID_GRUPO_PERMISSAO")})
    private List<GrupoPermissao> grupoPermissaos;

    @NotNull(message = "{wilsoftware.default.erro.dataInsert}")
    @Setter
    private LocalDateTime dateCreate;

    @NotNull(message = "{wilsoftware.default.erro.sessaoInsert}")
    @Setter
    private String sessaoCreate;

    @Setter
    private LocalDateTime dateUpdate;

    @Setter
    private String sessaoUpdate;

    @Setter
    @ApiModelProperty("Ordem do Grupo de Usuário")
    @NotNull(message = "{wilsoftware.grupo.exception.ordemNotNull}")
    private Integer ordem;

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}


