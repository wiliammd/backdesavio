package com.wilsoftware.notafiscal.grupoPermissao.domain;

import com.wilsoftware.notafiscal.UserService;
import com.wilsoftware.notafiscal.exceptions.BeanValidationException;
import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.exceptions.ValidatorService;
import com.wilsoftware.notafiscal.grupoPermissao.validators.GrupoPermissaoDeleteValidator;
import com.wilsoftware.notafiscal.grupoPermissao.validators.GrupoPermissaoInsertValidator;
import com.wilsoftware.notafiscal.grupoPermissao.validators.GrupoPermissaoUpdateValidator;
import com.wilsoftware.notafiscal.util.IdGenerator;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class GrupoPermissaoService {
    @Autowired
    private GrupoPermissaoRepository repository;

    @Autowired
    private ValidatorService service;

    private List<? extends GrupoPermissaoInsertValidator> insertValidators;
    private List<? extends GrupoPermissaoUpdateValidator> updateValidators;
    private List<? extends GrupoPermissaoDeleteValidator> deleteValidators;

    @Autowired
    public GrupoPermissaoService(ListableBeanFactory beanFactory) {
        insertValidators = new ArrayList<>(beanFactory.getBeansOfType(GrupoPermissaoInsertValidator.class).values());
        updateValidators = new ArrayList<>(beanFactory.getBeansOfType(GrupoPermissaoUpdateValidator.class).values());
        deleteValidators = new ArrayList<>(beanFactory.getBeansOfType(GrupoPermissaoDeleteValidator.class).values());
    }

    public GrupoPermissao insert(GrupoPermissao grupoPermissao) {
        grupoPermissao.setId(IdGenerator.get());
        grupoPermissao.setDateCreate(LocalDateTime.now());
        grupoPermissao.setSessaoCreate(UserService.authenticated().getId());
        if (service.validate(grupoPermissao).ifPresents()) {
            throw new BeanValidationException(service.get());
        }
        insertValidators.sort(Comparator.comparing(GrupoPermissaoInsertValidator::getOrder));
        insertValidators.forEach(validator -> validator.validate(grupoPermissao));

        return repository.save(grupoPermissao);
    }

    public GrupoPermissao update(GrupoPermissao grupoPermissao) {
        grupoPermissao.setDateCreate(LocalDateTime.now());
        grupoPermissao.setSessaoCreate(UserService.authenticated().getId());
        if (service.validate(grupoPermissao).ifPresents()) {
            throw new BeanValidationException(service.get());
        }
        updateValidators.sort(Comparator.comparing(GrupoPermissaoUpdateValidator::getOrder));
        updateValidators.forEach(validator -> validator.validate(grupoPermissao));
        return repository.save(grupoPermissao);
    }

    public GrupoPermissao delete(String id) {
        GrupoPermissao grupoPermissao = GrupoPermissao.builder().id(id).build();
        deleteValidators.sort(Comparator.comparing(GrupoPermissaoDeleteValidator::getOrder));
        deleteValidators.forEach(validator -> validator.validate(grupoPermissao));
        repository.deleteById(grupoPermissao.getId());
        return grupoPermissao;
    }

    public Page<GrupoPermissao> findAll(int page, int pageSize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        return repository.findAll(pageRequest);
    }

    public GrupoPermissao findById(String id) {
        String[] array = {id};
        return repository.findById(id).orElseThrow(() -> new ObjectNotFoundException("{wilsoftware.roles.exception.idNaoEncontrado}", array));
    }

}
