package com.wilsoftware.notafiscal.grupoPermissao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wilsoftware.notafiscal.commons.EntityBase;
import com.wilsoftware.notafiscal.grupo.domain.Grupo;
import com.wilsoftware.notafiscal.roles.domain.Roles;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "GRUPO_PERMISSAO")
public class GrupoPermissao implements EntityBase {
    @Id
    @ApiModelProperty("Id Interno do Atributo Grupo permissão")
    @Column(length = 40)
    private String id;

    @ApiModelProperty("Descrição do Grupo permissão")
    @Column(length = 50)
    private String descricao;

    @ApiModelProperty("Agrupador do Grupo permissão")
    @Column(length = 50)
    private String agrupador;

    @ManyToMany
    @JoinTable(name = "GRUPO_PERMISSAO_ROLES",
            joinColumns = {@JoinColumn(name = "ID_GRUPO_PERMISSAO")},
            inverseJoinColumns = {@JoinColumn(name = "ID_ROLES")})
    private List<Roles> roles;

    @JsonIgnore
    @ManyToMany(mappedBy = "grupoPermissaos")
    private List<Grupo> grupos;

    @NotNull(message = "{wilsoftware.default.erro.dataInsert}")
    private LocalDateTime dateCreate;

    @NotNull(message = "{wilsoftware.default.erro.sessaoInsert}")
    private String sessaoCreate;

    private LocalDateTime dateUpdate;

    private String sessaoUpdate;

}
