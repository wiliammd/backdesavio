package com.wilsoftware.notafiscal.grupoPermissao.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GrupoPermissaoRepository extends JpaRepository<GrupoPermissao, String> {

}
