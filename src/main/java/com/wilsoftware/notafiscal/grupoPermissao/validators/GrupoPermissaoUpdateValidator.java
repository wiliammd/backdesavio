package com.wilsoftware.notafiscal.grupoPermissao.validators;

import com.wilsoftware.notafiscal.commons.validator.Validator;
import com.wilsoftware.notafiscal.grupoPermissao.domain.GrupoPermissao;

public interface GrupoPermissaoUpdateValidator<EXCEPTION extends RuntimeException> extends Validator<GrupoPermissao, EXCEPTION> {
    Integer getOrder();
}
