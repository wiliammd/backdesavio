package com.wilsoftware.notafiscal.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class ConvertStringToLocalDate {


    public static LocalDateTime converterStringToLocalDate(String data) {

        String dataAux = data.substring(0, 4) + "-" + data.substring(4, 6) + "-" + data.substring(6, 8) + "T" + data.substring(8, 10) + ":" + data.substring(10, 12) + ":" + data.substring(12, 14) + "Z";
        Instant instant = Instant.parse(dataAux);
        LocalDateTime result = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);

        return result;

    }


}
