package com.wilsoftware.notafiscal.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class ConvertTokenToString {


    public static String converterToken(String authorization, String secret) {

        Claims claims = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(authorization).getBody();
        if (claims != null) {
            return claims.get("sessaoId", String.class);
        }
        return null;
    }


}
