package com.wilsoftware.notafiscal.roles.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wilsoftware.notafiscal.commons.EntityBase;
import com.wilsoftware.notafiscal.grupoPermissao.domain.GrupoPermissao;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ROLES")
public class Roles implements EntityBase {
    @Id
    @ApiModelProperty("Id Interno do Atributo Roles")
    @Column(length = 40)
    private String id;

    @ApiModelProperty("Descrição do Roles")
    @Column(length = 50)
    private String descricao;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private List<GrupoPermissao> grupoPermissaos;

    @NotNull(message = "{wilsoftware.default.erro.dataInsert}")
    private LocalDateTime dateCreate;

    @NotNull(message = "{wilsoftware.default.erro.sessaoInsert}")
    private String sessaoCreate;

    private LocalDateTime dateUpdate;

    private String sessaoUpdate;

}
