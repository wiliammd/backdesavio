package com.wilsoftware.notafiscal.roles.validators;

import com.wilsoftware.notafiscal.exceptions.ObjectNotFoundException;
import com.wilsoftware.notafiscal.roles.domain.Roles;
import com.wilsoftware.notafiscal.roles.domain.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RolesUpdateIdValidator implements RolesUpdateValidator<ObjectNotFoundException> {

    @Autowired
    RolesRepository repository;

    @Override
    public Integer getOrder() {
        return 0;
    }

    @Override
    public boolean isValid(Roles entity) {
        Optional<Roles> byId = repository.findById(entity.getId());
        if(byId.isPresent()){
            return true;
        }
        return false;
    }

    @Override
    public ObjectNotFoundException getException(Roles entity) {
       return new ObjectNotFoundException("{wilsoftware.roles.exception.idNaoEncontrado}");
    }
}
