package com.wilsoftware.notafiscal.roles.validators;

import com.wilsoftware.notafiscal.commons.validator.Validator;
import com.wilsoftware.notafiscal.roles.domain.Roles;

public interface RolesDeleteValidator<EXCEPTION extends RuntimeException> extends Validator<Roles, EXCEPTION> {
    Integer getOrder();
}
